define({
    root: {
        exampleTitle:"Exemple",
        drawTitleMenu :"Dessin",
        drawTitle :"Exemple Dessin",
        algeria:"Carte d'Algérie",
        satellite:"World Imagery",
        streets:"World Street Map",
        osm :"Open Street Map",
        commune:"Commune",
        wilaya:"Wilaya"
    },
    "ar": true
});