define([
        "esri/layers/FeatureLayer",
        "esri/tasks/query"
    ],
    (FeatureLayer, Query) => ({

        getStates() {

            var layer = new FeatureLayer('http://10.6.99.187:6080/arcgis/rest/services/OufokMapSevices/D%C3%A9coupage_SDA/MapServer/6', {
                mode: FeatureLayer.MODE_ONDEMAND,
                outFields: ["*"]
            });

            var query = new Query();
            query.where = "1=1";
            var data = [];

            var promise = new Promise((resolve, reject) => {

                layer.queryFeatures(query, function (response) {
                        resolve(response);
                    },
                    function (error) {
                        reject(Error(error));
                    });
            });

            return promise;

        },
        getCities() {

            const observable = Rx.Observable.create(function (observer) {

                var layer = new FeatureLayer('http://10.5.28.224:6080/arcgis/rest/services/FDP/LP10042014_WGS84/MapServer/20', {
                    mode: FeatureLayer.MODE_ONDEMAND,
                    outFields: ["*"]
                });

                var query = new Query();
                query.where = "1=1";
                var data = [];

                layer.queryFeatures(query, function (response) {

                        observer.next(response);
                        observer.complete();

                    },
                    function (error) {
                        reject(Error(error));
                    });
            });

            return observable;

        }


    }));