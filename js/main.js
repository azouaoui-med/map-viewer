define([
    "js/initWidgets",
    "config/mapConfig",
    "config/layerConfig",
    "config/searchConfig",
    "js/loader",
    "esri/dijit/Search",
    "esri/dijit/Scalebar",
    "js/events"

], function (initWidgets, mapConfig, layerConfig, searchConfig, loader, Search, Scalebar, events) {

    return {
        map: null,
        startup: function () {

            this.initLanguage();
            this.initMap();

            $(window).on('load', function () {
                loader.windowLoaded = true;
                if (loader.appLoaded) {
                    loader.onLoad();
                }
            });

        },
        initMap: function () {

            this.map = mapConfig.map;

            this.map.on("load", $.proxy(function () {

                initWidgets.startup(this.map);
                this.initLayer();
                this.initSearch();
                this.initScaleBar();

                events.map = this.map;
                events.initEvent();

            }, this));

        },
        initLayer: function () {

            var layers = layerConfig.layers
            this.map.addLayers(layers);

        },
        initSearch: function () {

            var searchContainer = document.createElement("div");
            $("#main").append(searchContainer);

            var search = new Search({
                map: this.map,
                sources: searchConfig.sources
            }, searchContainer);

            search.startup();
        },
        initScaleBar: function () {
            
            var scalebar = new Scalebar({
                map: this.map,
                attachTo: "bottom-right",
                // "dual" displays both miles and kilometers
                // "english" is the default, which displays miles
                // use "metric" for kilometers
                scalebarUnit: "dual"
            });
        },
        initLanguage: function () {

            if (localStorage.getItem('locale') == 'ar') {
                $('body').addClass('rightToLeft');

            } else {
                $('body').removeClass('rightToLeft');
            }
        }
    }


});