define([
    "dojo/i18n!config/nls/local",
    "esri/map",
    'esri/dijit/Basemap',
    "esri/dijit/BasemapLayer",
    "esri/basemaps",
    "esri/geometry/Extent"
], function (i18n, Map, Basemap, BasemapLayer, esriBasemaps, Extent) {

    //create a div with id=mapDiv where the map will be displayed
    $('<div/>', {
        id: 'mapDiv',
    }).appendTo('#main');

    // add a custom basemap to the esri basemaps list 
    // this is needed when you want to set a custom basemap (for ex: local basemap) as a default basemap
    esriBasemaps.defaultBasemap = {
        baseMapLayers: [{
            url: "http://10.5.28.48:6080/arcgis/rest/services/viewer/wilayaTile/MapServer"
        }],
        thumbnailUrl: "http://10.5.28.48:6080/arcgis/rest/services/viewer/wilayaTile/MapServer/tile/4/6/7",
        title: "Algeria tile"
    };
    // create costum level of details
    var lods = [{

            "level": 0,
            "resolution": 156543.03392800014,
            "scale": 591657527.591555
        }, {
            "level": 1,
            "resolution": 78271.51696399994,
            "scale": 295828763.795777
        }, {
            "level": 2,
            "resolution": 39135.75848200009,
            "scale": 147914381.897889
        }, {
            "level": 3,
            "resolution": 19567.87924099992,
            "scale": 73957190.948944
        }, {
            "level": 4,
            "resolution": 9783.93962049996,
            "scale": 36978595.474472
        }, {
            "level": 5,
            "resolution": 4891.96981024998,
            "scale": 18489297.737236
        }, {
            "level": 6,
            "resolution": 2445.98490512499,
            "scale": 9244648.868618
        }, {
            "level": 7,
            "resolution": 1222.992452562495,
            "scale": 4622324.434309
        }, {
            "level": 8,
            "resolution": 611.4962262813797,
            "scale": 2311162.217155
        }, {
            "level": 9,
            "resolution": 305.74811314055756,
            "scale": 1155581.108577
        }, {
            "level": 10,
            "resolution": 152.87405657041106,
            "scale": 577790.554289
        }, {
            "level": 11,
            "resolution": 76.43702828507324,
            "scale": 288895.277144
        }, {
            "level": 12,
            "resolution": 38.21851414253662,
            "scale": 144447.638572
        }, {
            "level": 13,
            "resolution": 19.10925707126831,
            "scale": 72223.819286
        }, {
            "level": 14,
            "resolution": 9.554628535634155,
            "scale": 36111.909643
        }, {
            "level": 15,
            "resolution": 4.77731426794937,
            "scale": 18055.954822
        }, {
            "level": 16,
            "resolution": 2.388657133974685,
            "scale": 9027.977411
        }, {
            "level": 17,
            "resolution": 1.1943285668550503,
            "scale": 4513.988705
        }, {
            "level": 18,
            "resolution": 0.5971642835598172,
            "scale": 2256.994353
        }, {
            "level": 19,
            "resolution": 0.29858214164761665,
            "scale": 1128.497176
        }, {
            "level": 20,
            "resolution": 0.14929107082380833,
            "scale": 564.248588
        },
        {
            "level": 21,
            "resolution": 0.07464553541190416,
            "scale": 282.124294
        },
        {
            "level": 22,
            "resolution": 0.03732276770595208,
            "scale": 141.062147
        },
        {
            "level": 23,
            "resolution": 0.01866138385297604,
            "scale": 70.5310735
        }
    ]
    return {
        //Create a new map 
        map: new Map("mapDiv", {
            basemap: "defaultBasemap", //set default basemap 
            extent: new Extent({
                "xmin": -1386703.1694781692,
                "ymin": 2814897.2825756585,
                "xmax": 2814897.2825756585,
                "ymax": 4610236.542610689,
                spatialReference: {
                    wkid: 102100
                }
            }),
            lods: lods
            // zoom: 5, // Sets zoom level based on level of detail (LOD)
            // center: [3.262939, 36.618283], // Sets center point of view using longitude,latitude
        }),
        // a list of custom basemaps that will be used in basemap gallery
        localBasemaps: [
            new Basemap({
                layers: [new BasemapLayer({
                    url: "http://10.6.99.187:6080/arcgis/rest/services/OufokMapSevices/FDP_500_1000_WGS/MapServer"
                })],
                id: "algeria",
                title: i18n.algeria + " 1000",
                thumbnailUrl: "img/algeria.png"
            }),
            new Basemap({
                layers: [new BasemapLayer({
                    url: "http://10.6.99.187:6080/arcgis/rest/services/OufokMapSevices/FDP1000_WGS84/MapServer"
                })],
                id: "algeria2",
                title: i18n.algeria + " 1000_500",
                thumbnailUrl: "img/algeria.png"
            })
        ],
        // a list of online basemaps that will be used in basemap gallery
        onlineBasemaps: [
            new Basemap({
                layers: [new BasemapLayer({
                    url: "http://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer"
                })],
                id: "satellite",
                title: i18n.satellite,
                thumbnailUrl: "http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/4/5/4"
            }),
            new Basemap({
                layers: [new BasemapLayer({
                    type: 'WebTiledLayer',
                    url: "https://{subDomain}.tile.openstreetmap.org/{level}/{col}/{row}.png",
                    subDomains: ["a", "b", "c"]
                })],
                id: "osm",
                title: i18n.osm,
                thumbnailUrl: "https://a.tile.openstreetmap.org/6/31/25.png"
            }),
            new Basemap({
                layers: [new BasemapLayer({
                    type: 'WebTiledLayer',
                    url: "http://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{level}/{row}/{col}.png"
                })],
                id: "streets",
                title: i18n.streets,
                thumbnailUrl: "https://stamen-tiles.a.ssl.fastly.net/terrain/10/177/409.png"
            })
            // ,
            // new Basemap({
            //     layers: [new BasemapLayer({
            //         type: 'WebTiledLayer',
            //         url: "http://{subDomain}.tile.stamen.com/terrain/{level}/{col}/{row}.png",
            //         subDomains: ["a", "b", "c", "d"]
            //     })],
            //     id: "stamen",
            //     title: "Stamen",
            //     thumbnailUrl: "http://a.tile.stamen.com/terrain/6/31/25.png"
            // })
        ]

    }
});