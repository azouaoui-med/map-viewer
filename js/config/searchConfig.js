define([
    "dojo/i18n!config/nls/local",
    "esri/layers/FeatureLayer",

], function (i18n, FeatureLayer) {

    return {
        // the feature layers needs to be published in 10.3 or higher version of arcgis server
        sources: [{
                featureLayer: new FeatureLayer("http://10.5.28.224:6080/arcgis/rest/services/FDP/LP10042014_WGS84/MapServer/20"),
                searchFields: ["nom"],
                suggestionTemplate: "${nom}",
                exactMatch: false,
                outFields: ["*"],
                name: i18n.commune,
                //labelSymbol: textSymbol,
                placeholder: i18n.commune,
                maxResults: 6,
                maxSuggestions: 6,
                enableSuggestions: true,
                minCharacters: 0,
                localSearchOptions: {
                    distance: 5000
                }
            },
            {
                featureLayer: new FeatureLayer("http://10.5.28.224:6080/arcgis/rest/services/FDP/LP10042014_WGS84/MapServer/21"),
                searchFields: ["WILAYA"],
                suggestionTemplate: "${code_wilaya} - ${WILAYA}",
                exactMatch: false,
                outFields: ["*"],
                name: i18n.wilaya,
                //labelSymbol: textSymbol,
                placeholder: i18n.wilaya,
                maxResults: 6,
                maxSuggestions: 6,
                enableSuggestions: true,
                minCharacters: 0,
                localSearchOptions: {
                    distance: 5000
                }
            }


        ]

    }
});